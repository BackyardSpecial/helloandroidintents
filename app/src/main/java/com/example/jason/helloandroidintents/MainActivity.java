package com.example.jason.helloandroidintents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;

        int id = item.getItemId();
        switch (id){
            case R.id.OtherActivityOption:
                this.StartOtherActivity();
                handled = true;
                break;
            case R.id.StartOtherServiceOption:
                this.StartOtherService();
                handled = true;
                break;
            case R.id.StopOtherServiceOption:
                this.StopOtherService();
                handled = true;
                break;
            case R.id.quit:
                handled = true;
                break;
            default:
                handled = super.onOptionsItemSelected(item);
                break;
        }

        return handled;
    }

    public void StartOtherActivity()
    {
        Intent intent = new Intent(this,OtherActivity.class);
        startActivity(intent);
    }

    public void StartOtherService()
    {
        Intent intent = new Intent("com.example.jason.helloandroidintents.action.LOG_TIME");
        startService(intent);
    }

    public void StopOtherService()
    {

    }
}
