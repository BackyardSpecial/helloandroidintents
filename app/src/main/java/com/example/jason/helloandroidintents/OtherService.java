package com.example.jason.helloandroidintents;

import android.app.Service;
import android.content.Intent;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.util.Log;

import java.util.Date;

public class OtherService extends Service {

    private static final String TAG = OtherService.class.getSimpleName();

    public OtherService() {
    }

    @Override
    public IBinder onBind(Intent intent) {

        Log.i(TAG, "onBind, Thread name" + Thread.currentThread().getName());

        // return null so that no binding can occur.
        return null;
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate, Thread name" + Thread.currentThread().getName());
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy, Thread name" + Thread.currentThread().getName());
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i(TAG, "onStart, Thread name" + Thread.currentThread().getName());

        SimpleDateFormat simpleDateFormat = null;

        String action = intent.getAction();

        if(action.equals("com.example.jason.helloandroidintents.action.LOG_TIME")) {
            simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        }
        else if(action.equals("com.example.jason.helloandroidintents.action.LOG_DATE")) {
            simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd");
        }
        else{
            Log.i(TAG, "missing or unrecognized action.");
        }

        if(simpleDateFormat != null)
        {
            long now = (new Date()).getTime();
            Log.i(TAG, simpleDateFormat.format(now));
        }

        return super.onStartCommand(intent, flags, startId);
    }


}
